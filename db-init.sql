drop table if exists `Call`;
create table `Call` (
`id` bigint(30) unsigned not null auto_increment,
`call_ref` varchar(64) not null,
`secure` bool not null default false,
`current_stage` bigint(30) unsigned,
`current_item` bigint(30) unsigned,
`createAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY(`id`),
UNIQUE KEY(`call_ref`)
);

drop table if exists `CallLeg`;
create table `CallLeg` (
`id` bigint(30) unsigned not null auto_increment,
`call_id` bigint(30) unsigned not null,
`call_leg_ref` varchar(64) not null,
`sip_call_id`  varchar(128) not null,
`direction`    ENUM('INBOUND', 'OUTBOUND') not null,
`profile`      varchar(64) not null,
`createAt`     TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY(`id`),
KEY(`call_id`),
UNIQUE KEY(`call_leg_ref`)
);
desc `CallLeg`;


drop table if exists `CallLeg_Digits`;
create table `CallLeg_Digits`(
`id` bigint(30) unsigned not null auto_increment,
`call_leg_ref` varchar(64) not null,
`digits` varchar(64) default null,
`updateAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY(`id`),
UNIQUE KEY(`call_leg_ref`)
);
desc `CallLeg_Digits`;

show processlist;
kill 516;
SHOW VARIABLES LIKE "max_connections"; 

use SecureSip;
show tables;
SELECT * from `Call`;
SELECT * from `CallLeg`;
SELECT * from `CallLeg_Digits`;

SELECT A.*, B.`call_ref` from `CallLeg` as A, `Call` As B where A.`call_id`=B.`id` and B.`call_ref`='e67a3c71-32f0-4c7a-8704-1ec89a040fa7' and A.`call_leg_ref` != 'e67a3c71-32f0-4c7a-8704-1ec89a040fa7';

SELECT * from `Call` INNER JOIN `CallLeg` ON `Call`.`id`=`CallLeg`.`call_id` INNER JOIN `CallLeg_Digits` ON `CallLeg_Digits`.`call_leg_ref`=`CallLeg`.`call_leg_ref` where `Call`.`call_ref`='b80293bc-d4a3-47a5-967e-123a6ef3fdc9';
SET SQL_SAFE_UPDATES = 0;
explain DELETE `Call`, `CallLeg`, `CallLeg_Digits` FROM `Call` INNER JOIN `CallLeg` ON `Call`.`id`=`CallLeg`.`call_id` INNER JOIN `CallLeg_Digits` ON `CallLeg_Digits`.`call_leg_ref`=`CallLeg`.`call_leg_ref` where `Call`.`call_ref`= '2f245982-9cb2-4473-8e6c-f5c02066d5a8';
use SecureSip;
truncate table `Call`;
truncate table `CallLeg`;
truncate table `CallLeg_Digits`;
SELECT * from `Call` where `call_ref`= "75016520-4738-44c5-a891-9cfd4d8a00f9";

use SecureSip;
desc `CallLeg_Digits`;


-- drop table if exists `Session`;
-- create table `Session`(
-- `id` bigint(30) unsigned not null auto_increment,
-- `secure` bool not null default false,
-- `current_item` bigint(30) unsigned not null,
-- `createAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
-- PRIMARY KEY(`id`),
-- KEY(`createAt`)
-- );
-- desc `Session`;

drop table if exists `SessionItem`;
create table `SessionItem`(
`id` bigint(30) unsigned not null auto_increment,
`call_ref` varchar(64) not null,
`item_id` bigint(30) unsigned not null,
`item_value` varchar(64) not null,
PRIMARY KEY(`id`),
KEY(`call_ref`)
);
desc `SessionItem`;

drop table if exists `StageConfiguration`;
create table `StageConfiguration` (
`id` bigint(30) unsigned not null auto_increment,
`stage` int unsigned not null,
`description` varchar(128) not null,
`mode` enum('normal', 'secure') not null,
PRIMARY KEY(`id`),
UNIQUE KEY(`stage`)
);
insert into `StageConfiguration` values 
(1, 1, 'Call reference number', 'normal'), 
(2, 2, 'Credit Card number', 'secure'), 
(3, 3, 'CVV', 'secure'), 
(4, 4, 'Expiry', 'normal'),
(5, 5, 'Driver License', 'secure')
;

drop table if exists `ItemConfiguration`;
create table `ItemConfiguration`(
`id` bigint(30) unsigned not null auto_increment,
`name` varchar(64) not null,
`description` varchar(128) not null,
`pattern` varchar(32) not null,
`replacement` varchar(32) default null,
`stage` int unsigned not null,
`priority` int unsigned not null,
`call_party` enum('A_party', 'B_party', 'Any_party') not null,
`next_item` bigint(30) unsigned default null,
PRIMARY KEY(`id`),
KEY(`stage`)
);
insert into `ItemConfiguration` values 
(1, 'CRN', 'Call reference number', '^##\\d{6}$', null, 1, 1, 'Any_party', null), 
(2, 'CreditCard', 'Visa', '^\\d{16}$', null, 2, 2, 'Any_party', 4), 
(3, 'CreditCard', 'Amex', '^3\\d{14}$', null,2, 1, 'Any_party', 5), 
(4, 'CVV', 'Visa CVV', '^\\d{3}$', null, 3, 1, 'Any_party', null), 
(5, 'CVV', 'Amex CVV', '^\\d{4}$', null, 3, 1, 'Any_party', null), 
(6, 'Expiry', 'Expiry', '^\\d{4}$', null, 4, 1, 'Any_party', null),
(7, 'DriverLicense', 'Driver License', '^(\\d+)(#)$', '$1', 5, 1, 'Any_party', null)
-- (1, 'CRN', 'Call reference number', '^##\\d{6}$', 1, 1, 'B_party', null), 
-- (2, 'CreditCard', 'Visa', '^\\d{16}$', 2, 2, 'A_party', 4), 
-- (3, 'CreditCard', 'Amex', '^3\\d{14}$', 2, 1, 'A_party', 5), 
-- (4, 'CVV', 'Visa CVV', '^\\d{3}$', 3, 1, 'A_party', null), 
-- (5, 'CVV', 'Amex CVV', '^\\d{4}$', 3, 1, 'A_party', null), 
-- (6, 'Expiry', 'Expiry', '^\\d{4}$', 4, 1, 'A_party', null)
;
desc `ItemConfiguration`;
select * from `ItemConfiguration`;

drop table if exists `SystemConfiguration`;
create table `SystemConfiguration` (
`id` bigint(30) unsigned not null auto_increment,
`name` varchar(64) not null,
`value` varchar(128) not null,
PRIMARY KEY(`id`),
KEY(`name`)
);
insert into `SystemConfiguration` values
(1, 'DigitMask', '*'),
(2, 'DtmfDuration', 250)
;
select * from `SystemConfiguration`;
