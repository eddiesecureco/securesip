import "reflect-metadata";
import { createConnection, getConnectionOptions, ConnectionOptions} from "typeorm";
import { CallDao } from "./database/call-dao";
import { CallLegDao } from "./database/call-leg-dao";
import { CallLegDigitsDao } from "./database/call-leg-digits-dao";
import { ItemConfigurationDao } from "./database/item-configuration-dao";
import { StageConfigurationDao } from "./database/stage-configuration-dao";
import { SystemConfigurationDao } from "./database/system-configuration-dao";
import { SessionItemDao } from "./database/session-item-dao";
import { EventController } from "./controllers/event-controller";

var initialised: boolean = false;

async function load(): Promise<ConnectionOptions|undefined> {
    // We should only load config from environment.
    let connectionOptions = await getConnectionOptions();
    if (connectionOptions) {
        return connectionOptions;
    } else {
        console.log("connectionOptions NOT Found!");
    }

    return undefined;
}

async function init() {
    if (initialised)
        return;
    initialised = true;
    var options = await load();
    console.log("Initializing DB Connection ...");
    var connection = await createConnection(options);
    console.log("Connection initialised.");
    
    // Initialise all the DAOs.
    CallDao.initialize(connection);
    CallLegDao.initialize(connection);
    CallLegDigitsDao.initialize(connection);
    ItemConfigurationDao.initialize(connection);
    StageConfigurationDao.initialize(connection);
    SystemConfigurationDao.initialize(connection);
    SessionItemDao.initialize(connection);
}

export async function handler(event:any, context:any, callback:any) {
    await init();
    var request = JSON.parse(event.body);
    context.callbackWaitsForEmptyEventLoop = false;

    console.log('request=' + JSON.stringify(request));
    var resp = await EventController.process_event(request);
    console.log('response=' + JSON.stringify(resp));
    callback(null, resp);
}
