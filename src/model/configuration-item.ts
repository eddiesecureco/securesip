export enum ConfigurationItem {
    DIGIT_MASK = "DigitMask",
    DTMF_DURATION = "DtmfDuration"
}