import { SipEventType } from "../model/sip-event-type";
import { CallDirection } from "../model/call-direction";
import { DigitInfo } from "../model/digit-info";
import { Timestamp } from "typeorm";

export class SipEvent {
    event: SipEventType;

    call_leg_ref: string;

    call_ref: string;

    sip_call_id: string;

    profile: string;

    eventTimestamp: Timestamp;

    direction: CallDirection;

    digitInfo: DigitInfo;
    
}