export interface EventResponse {
    statusCode: number;
    body: string;
}