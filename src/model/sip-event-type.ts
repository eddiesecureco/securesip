export enum SipEventType {
    CALL_CREATE = "CALL_CREATE",
    ANSWER = "ANSWER",
    RELEASE = "RELEASE",
    DIGIT = "DIGIT" 
}