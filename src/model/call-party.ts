export enum CallParty {
    A_PARTY = "A_party",
    B_PARTY = "B_party",
    ANY     = "Any_party"
}