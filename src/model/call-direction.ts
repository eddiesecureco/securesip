export enum CallDirection {
    INBOUND = "INBOUND",
    OUTBOUND = "OUTBOUND"
}