import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

/**
 * Store the information of a "call".
 * Typically, a "call" would have two call legs - incoming and outgoing.
 * See @{CallLeg} for details.
 */
@Entity("Call")
export class Call {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    call_ref: string;

    @Column()
    secure: boolean;

    @Column()
    current_stage: number;

    @Column()
    current_item: number;

    @Column()
    call_start: number;

    @Column()
    call_end: number;

    @Column()
    call_answer: number;

    @Column()
    call_duration_ms: number;
}