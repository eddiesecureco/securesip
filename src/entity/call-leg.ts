import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { CallDirection } from "../model/call-direction";

@Entity("CallLeg")
export class CallLeg {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    call_id: number;    // This is a reference key => id of Call table

    @Column()
    call_leg_ref: string;   // This is a call leg UUID used in freeswitch

    @Column()
    sip_call_id: string;    // This is the SIP Call-ID field

    @Column()
    direction: CallDirection;   // inbound/outbound

    @Column()
    profile: string;       // Call profile being used by freeswitch
}