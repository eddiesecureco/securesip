import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { CallParty } from "../model/call-party";

@Entity("ItemConfiguration")
export class ItemConfiguration {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    name: string;   

    @Column()
    description: string;  

    @Column()
    pattern: string;

    @Column()
    replacement: string;

    @Column()
    stage: number;
    
    @Column()
    priority: number;

    @Column()
    call_party: CallParty;

    @Column()
    next_item: number;
}