import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity("CallLeg_Digits")
export class CallLegDigits {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    call_leg_ref: string;

    @Column()
    digits: string;
}