import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity("SessionItem")
export class SessionItem {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    call_ref: string;   

    @Column()
    item_id: number;  

    @Column()
    item_value: string;
}