import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { StageMode } from "../model/stage-mode";

@Entity("StageConfiguration")
export class StageConfiguration {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    stage: number;   

    @Column()
    description: string;  

    @Column()
    mode: StageMode;
}