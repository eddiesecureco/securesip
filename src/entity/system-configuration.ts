import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("SystemConfiguration")
export class SystemConfiguration {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    value: string;
}