import { Call } from "../entity/call";
import { getConnection, Repository, TransactionManager, Entity, Connection} from "typeorm";
import { CallLeg } from "../entity/call-leg";
import { CallDirection } from "../model/call-direction";

export module CallLegDao {
    let callLegRepository: Repository<CallLeg>;
    let callRepository: Repository<Call>;

    export const initialize = (connection: Connection) => {
        callLegRepository = connection.getRepository(CallLeg);
        callRepository = connection.getRepository(Call);
    };

    export async function add(call_id:number, direction:CallDirection, profile:string, call_leg_ref:string, sip_call_id:string):Promise<CallLeg|null> {

        var callLegEntity:CallLeg = new CallLeg();
        callLegEntity.call_id = call_id;
        callLegEntity.direction = direction;
        callLegEntity.profile = profile;
        callLegEntity.call_leg_ref = call_leg_ref;
        callLegEntity.sip_call_id = sip_call_id;

        return new Promise(resolve => {
            callLegRepository.save(callLegEntity).then((result) => {
                console.log("Call Leg Ref " + call_leg_ref + " -> Call Leg ID " + result.id);
                resolve(result);
            }).catch(error => {
                console.log(error)
                resolve(null);
            });      
        });
    }

    export async function get( call_leg_ref:string):Promise<CallLeg|null> {
        if (call_leg_ref == null || call_leg_ref.length == 0) {
            console.log("Invalid null call_leg_ref!!!");
            return new Promise(resolve => {
                resolve(null)
            });
        }

        return new Promise(resolve => {
            var callLegEntity:CallLeg = new CallLeg();
            callLegEntity.call_leg_ref = call_leg_ref;
            callLegRepository.findOne(callLegEntity).then((result) => {
                resolve(result);
            }).catch(error => {
                console.log(error)
                resolve(null);
            });
        });
    }


    export async function findOrCreate(call_id:number, direction:CallDirection, profile:string, call_leg_ref:string, sip_call_id:string):Promise<CallLeg|null> {
        if (call_leg_ref == null || call_leg_ref.length == 0 || sip_call_id == null || sip_call_id.length == 0) {
            console.log("Invalid null call_leg_ref or sip_call_id!!!");
            return new Promise(resolve => {
                resolve(null)
            });
        }

        return new Promise(resolve => {
            callLegRepository.manager.transaction( async transactionManager => {
                var result:CallLeg = await get(call_leg_ref);
                if (result == null) {
                    console.log("Creating a call leg object");
                    result = await add(call_id, direction, profile, call_leg_ref, sip_call_id);
                    resolve(result);
                } else {
                    resolve(result);
                }
            });
        });
    }


    export async function getOtherLegs(call_ref:string, call_leg_ref:string):Promise<CallLeg|null> {
        if (call_leg_ref == null || call_leg_ref.length == 0 || call_ref == null || call_ref.length == 0) {
            console.log("Invalid null call_leg_ref/call_ref!!!");
            return new Promise(resolve => {
                resolve(null)
            });
        }
        return new Promise(resolve => {
            var callEntity:Call = new Call();
            callEntity.call_ref = call_ref;
            callRepository.findOne(callEntity).then(
                (callResult) => {
                    if (callResult == null) {
                        console.log('Call not found, ref=' + call_ref);
                        resolve(null);
                        return;
                    }
                    var callLegEntity:CallLeg = new CallLeg();
                    callLegEntity.call_id = callResult.id;
                    callLegRepository.find(callLegEntity).then(
                        (results:CallLeg[]) => {
                            results.forEach(cl=>{
                                if (cl.call_leg_ref !== call_leg_ref) {
                                    resolve(cl);
                                    return;
                                }
                            });

                            // Not found
                            resolve(null);
                        }
                    ).catch(error => {
                        console.log(error)
                        resolve(null);
                    });
                }
            );
        });
    }

    export async function remove(call_leg_ref:string):Promise<CallLeg|null> {
        if (call_leg_ref == null || call_leg_ref.length == 0) {
            console.log("Invalid null call_leg_ref!!!");
            return new Promise(resolve => {
                resolve(null)
            });
        }
        return new Promise(resolve => {
            get(call_leg_ref).then(result => {
                if (result == null) {
                    console.log("Call Leg not found, ref=" + call_leg_ref);
                    resolve(null);
                    return;
                }
                callLegRepository.delete(result.id);
                resolve(result);
            }).catch(error => {
                console.log(error)
                resolve(null);
            });
        });
    }
}