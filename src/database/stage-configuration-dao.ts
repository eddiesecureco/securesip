import { getConnection, Repository, Connection } from "typeorm";
import { StageConfiguration } from "../entity/stage-configuration";

export module StageConfigurationDao {
    let stageConfigRepository: Repository<StageConfiguration>;

    export const initialize = (connection: Connection) => {
        stageConfigRepository = connection.getRepository(StageConfiguration);
    };


    /**
     * Get the Item config by id
     */
    export async function getById(id:number):Promise<StageConfiguration|null> {
        return new Promise((resolve) => {
            var stageConfig:StageConfiguration = new StageConfiguration();
            stageConfig.id = id;
            stageConfigRepository.findOne(stageConfig).then(
                (ic) => {
                    console.log("Retrieved stage config object ... id=" + id + " Found=" + (ic != null));
                    resolve(ic);
                }
            )
        });
    }

    /**
     * find item by stage
     */
    export async function findByStage(stage: number):Promise<StageConfiguration|null> {
        return new Promise((resolve) => {
            var stageConfig:StageConfiguration = new StageConfiguration();
            stageConfig.stage = stage;       // find by order
            stageConfigRepository.find(stageConfig).then(
                (ic) => {
                    // Stage number is an unique key.  So, either not found or just one result.
                    if (ic && ic.length == 1) {
                        resolve(ic[0]);
                    } else {
                        resolve(null);
                    }
                }
            )
        });
    }
   
}