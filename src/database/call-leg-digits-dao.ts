import { getConnection, Repository, Connection } from "typeorm";
import { CallLeg } from "../entity/call-leg";
import { CallLegDigits } from "../entity/call-leg-digits";

export module CallLegDigitsDao {
    let callLegDigitsRepository: Repository<CallLegDigits>;
    // let callLegRepository: Repository<CallLeg>;
    // let callRepository: Repository<Call>;

    export const initialize = (connection: Connection) => {
        callLegDigitsRepository = connection.getRepository(CallLegDigits);
        // callLegRepository = connection.getRepository(CallLeg);
        //   callRepository = connection.getRepository(Call);
    };


    /**
     * Query the db to find the call id from the call reference.
     * If not found, will insert a new entry into db.
     */
    export async function get(call_leg_ref:string):Promise<CallLegDigits|null> {
        if (call_leg_ref == null || call_leg_ref.length == 0) {
            console.log("Invalid null call_leg_ref!!!");
            return new Promise(resolve => {
                resolve(null)
            });
        }
        return new Promise((resolve) => {
            var callLegDigits:CallLegDigits = new CallLegDigits();
            callLegDigits.call_leg_ref = call_leg_ref;
            callLegDigitsRepository.findOne(callLegDigits).then(
                (cld) => {
                    console.log("Retrieved call leg digit object ... result=" + JSON.stringify(cld));
                    resolve(cld);
                }
            )
        });
    }

    export async function add(call_leg_ref:string, digits:string):Promise<CallLegDigits|null> {
        var cld:CallLegDigits = new CallLegDigits();
        cld.digits = digits;
        cld.call_leg_ref = call_leg_ref;
        return new Promise(
            (resolve) => {
                callLegDigitsRepository.save(cld).then(
                    (result) => {
                        resolve(cld);
                    }
                )
            }
        );
    }

    export async function remove(call_leg_ref:string):Promise<CallLegDigits|null> {
        if (call_leg_ref == null || call_leg_ref.length == 0) {
            console.log("Invalid null call_leg_ref!!!");
            return new Promise(resolve => {
                resolve(null)
            });
        }

        return new Promise(
            (resolve) => {
                get(call_leg_ref).then(
                    (cld) => {
                        if (cld == null) {
                            console.log("Call Leg digit not found, ref=" + call_leg_ref);
                            resolve(null);
                            return;
                        }
                        callLegDigitsRepository.remove(cld).then(
                            (removed) => {
                                resolve(cld);
                            }
                        )
                    }
                )
            }
        )
    }

    export async function save(callLegDigits: CallLegDigits):Promise<CallLegDigits|null> {
        return new Promise(resolve => {
            callLegDigitsRepository.save(callLegDigits).then((result) => {
                resolve(result);
            });
        });
    }

    export async function updateDigits(call_leg_ref:string, appendDigit:string):Promise<CallLegDigits|null> {
        if (call_leg_ref == null || call_leg_ref.length == 0 || appendDigit == null || appendDigit.length == 0) {
            console.log("Invalid null call_leg_ref or appendDigit!!!");
            return new Promise(resolve => {
                resolve(null)
            });
        }

        return new Promise(resolve => {
            callLegDigitsRepository.manager.transaction( async transactionManager => {
                var result:CallLegDigits = await get(call_leg_ref);
                if (result == null) {
                    console.log("Creating a call leg digits object");
                    result = await add(call_leg_ref, appendDigit);
                    resolve(result);
                } else {
                    // Append the new digit to the existing digit string.
                    result.digits += appendDigit;
                    result = await callLegDigitsRepository.manager.save(result);
                    resolve(result);
                }
            });
        });
    }
}