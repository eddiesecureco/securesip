import { Repository, Connection } from "typeorm";
import { SessionItem } from "../entity/session-item";

export module SessionItemDao {
    let sessionItemRepository: Repository<SessionItem>;

    export const initialize = (connection: Connection) => {
        sessionItemRepository = connection.getRepository(SessionItem);
    };


    /**
     * Get the session item by id
     */
    export async function getById(id:number):Promise<SessionItem|null> {
        return new Promise((resolve) => {
            var item:SessionItem = new SessionItem();
            item.id = id;
            sessionItemRepository.findOne(item).then(
                (si) => {
                    console.log("Retrieved item object ... result=" + JSON.stringify(si));
                    resolve(si);
                }
            )
        });
    }

    /**
     * Add a new session with the given starting item to collect.
     * @param current_item 
     */
    export async function add(call_ref: string, item_id: number, item_value: string):Promise<SessionItem|null> {

        var item:SessionItem = new SessionItem();
        item.call_ref = call_ref;
        item.item_id = item_id;
        item.item_value = item_value;

        return save(item);
    }

        /**
     * Add a new session with the given starting item to collect.
     * @param current_item 
     */
    export async function save(item: SessionItem):Promise<SessionItem|null> {
        return new Promise(resolve => {
            sessionItemRepository.save(item).then((result) => {
                console.log("Session item created.  id=" + result.id);
                resolve(result);
            }).catch(error => {
                console.log(error)
                resolve(null);
            });        
        });
    }
}