import { Call } from "../entity/call";
import { Repository, Connection} from "typeorm";
import { StageMode } from "../model/stage-mode";
import { StageConfiguration } from "../entity/stage-configuration";

export module CallDao {
    let repository: Repository<Call>;

    export const initialize = (connection: Connection) => {
        repository = connection.getRepository(Call);
    };

    export async function add(call_ref:string, stageConfig: StageConfiguration):Promise<Call|null> {
        var callEntity:Call = new Call();
        callEntity.call_ref = call_ref;
        callEntity.current_stage = stageConfig.stage;
        callEntity.secure = (stageConfig.mode === StageMode.SECURE);
        
        return new Promise(resolve => {
            repository.save(callEntity).then((result) => {
                console.log("Call Ref " + call_ref + " -> Call ID " + result.id);
                resolve(result);
            }).catch(error => {
                console.log(error)
                resolve(null);
            });        
        });
    }

    export async function save(callEntity: Call):Promise<Call|null> {
        return new Promise(resolve => {
            repository.save(callEntity).then((result) => {
                console.log("Call Object saved: Call Ref=" + callEntity.call_ref);
                resolve(result);
            }).catch(error => {
                console.log(error)
                resolve(null);
            });        
        });
    }

    /**
     * Query the db to find the call id from the call reference.
     * If not found, will insert a new entry into db.
     */
    export async function get(call_ref:string):Promise<Call|null> {
        if (call_ref == null || call_ref.length == 0) {
            console.log("Invalid null call_ref!!!");
            return new Promise(resolve => {
                resolve(null)
            });
        }
        return new Promise(resolve => {
            var callEntity:Call = new Call();
            callEntity.call_ref = call_ref;
            console.log("Finding call_ref=" + call_ref);
            repository.findOne(callEntity).then((result) => {
                    resolve(result);
                }).catch(error => {
                    console.log(error)
                    resolve(null);
                });
            });
    }

    /**
     * Query the db to find the call from id.
     */
    export async function findById(call_id:number):Promise<Call|null> {
        
        return new Promise(resolve => {
            var callEntity:Call = new Call();
            callEntity.id = call_id;
            console.log("Finding call id=" + call_id);
            repository.findOne(callEntity).then((result) => {
                    resolve(result);
                }).catch(error => {
                    console.log(error)
                    resolve(null);
                });
            });
    }
    
    export async function remove(call_id:number):Promise<number|null> {
        return new Promise(resolve => {
            repository.delete(call_id).then(
                ()=>resolve(call_id)
            ).catch(error => {
                console.log(error)
                resolve(null);
            });
        });
    }
}
