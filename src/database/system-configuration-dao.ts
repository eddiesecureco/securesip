import { Repository, Connection } from "typeorm";
import { SystemConfiguration } from "../entity/system-configuration";

export module SystemConfigurationDao {
    let configRepository: Repository<SystemConfiguration>;

    export const initialize = (connection: Connection) => {
        configRepository = connection.getRepository(SystemConfiguration);
    };


    /**
     * Get all Items
     */
    export async function getConfig():Promise<Map<string, string>|null> {
        return new Promise((resolve) => {
            configRepository.find().then(
                (allItems) => {
                    var result = new Map<string, string>();
                    console.log("Retrieved config size=" + allItems.length);
                    allItems.forEach(item=> {
                        result.set(item.name, item.value);
                    });
                    resolve(result);
                }
            )
        });
    }   
}