import { getConnection, Repository, Connection } from "typeorm";
import { ItemConfiguration } from "../entity/item-configuration";

export module ItemConfigurationDao {
    let itemConfigRepository: Repository<ItemConfiguration>;

    export const initialize = (connection: Connection) => {
        itemConfigRepository = connection.getRepository(ItemConfiguration);
    };


    /**
     * Get the Item config by id
     */
    export async function getById(id:number):Promise<ItemConfiguration|null> {
        return new Promise((resolve) => {
            var itemConfig:ItemConfiguration = new ItemConfiguration();
            itemConfig.id = id;
            itemConfigRepository.findOne(itemConfig).then(
                (ic) => {
                    console.log("Retrieved item config object ... id=" + id + " Found=" + (ic != null));
                    resolve(ic);
                }
            )
        });
    }

    /**
     * find items by stage
     */
    export async function findByStage(stage: number):Promise<ItemConfiguration[]|null> {
        return new Promise((resolve) => {
            var itemConfig:ItemConfiguration = new ItemConfiguration();
            itemConfig.stage = stage;       // find by order
            itemConfigRepository.find(itemConfig).then(
                (ic) => {

                    // Then sort the result by priority
                    var sortedResult = ic.sort((ic1, ic2) => {
                        if (ic1.priority === ic2.priority) return 0;
                        return ic1.priority > ic2.priority ? 1 : -1;
                    });

                    console.log("Retrieved item config objects ... result=" + JSON.stringify(sortedResult));
                    resolve(sortedResult);
                }
            )
        });
    }
   
}