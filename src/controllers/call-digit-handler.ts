import { Constants } from "../utils/constants";
import { CallDao } from "../database/call-dao";
import { CallLegDao } from "../database/call-leg-dao";
import { CallLegDigitsDao } from "../database/call-leg-digits-dao";
import { SipEvent } from "../model/sip-event"
import { ItemConfiguration } from "../entity/item-configuration";
import { ItemConfigurationDao } from "../database/item-configuration-dao";
import { SessionItemDao } from "../database/session-item-dao";
import { CallDirection } from "../model/call-direction";
import { CallParty } from "../model/call-party";
import { StageConfigurationDao } from "../database/stage-configuration-dao";
import { CallLegDigits } from "../entity/call-leg-digits";
import { Call } from "../entity/call";
import { SystemConfigurationDao } from "../database/system-configuration-dao";
import { ConfigurationItem } from "../model/configuration-item";
import { StageMode } from "../model/stage-mode";
import { EventHandler } from "./event-handler";
import { EventResponse } from "../model/event-response";
import { SystemUtils } from "../utils/system-utils";

export class CallDigitHandler implements EventHandler {
    async handle(event: SipEvent): Promise<EventResponse> {
        console.log("process_call_digit");
        if (event.digitInfo == null || event.digitInfo.digit == null) {
            return Constants.success_resp;
        }

        // Find all call legs
        var thisLeg = await CallLegDao.get(event.call_leg_ref);
        var otherLeg = await CallLegDao.getOtherLegs(event.call_ref, event.call_leg_ref);
        if (otherLeg == null || thisLeg == null) {
            console.log("Warning: Ignoring call dtmf digits because call leg not found!");
            return Constants.success_resp;
        }
        console.log("This leg=" + thisLeg.call_leg_ref + " other leg=" + otherLeg.call_leg_ref);

        // Find call object
        var callObj = await CallDao.findById(thisLeg.call_id);
        if (callObj == null) {
            console.log("Warning: Ignoring call dtmf digits because call object not found!");
            return Constants.success_resp;
        }

        // Find the current item if exists.
        var items: ItemConfiguration[];
        if (callObj.current_item) {
            items = [await ItemConfigurationDao.getById(callObj.current_item)];
        } else if (callObj.current_stage) {
            items = await ItemConfigurationDao.findByStage(callObj.current_stage);
        } else {
            // No item collection required.
            console.log("No item configuration is set.  Ignoring digit.  Call ref: " + event.call_leg_ref);
            items = [];
        }

        // Retrieve system configuration
        var configItems = await SystemConfigurationDao.getConfig();
        let maskedDigit = SystemUtils.getSystemConfig(configItems, ConfigurationItem.DIGIT_MASK, "*");   
        let dtmf_duration =  Number(SystemUtils.getSystemConfig(configItems, ConfigurationItem.DTMF_DURATION, "250"));

        // Pass thru digits unless secure pattern is detected.
        var outDigit = callObj.secure ? maskedDigit : event.digitInfo.digit;   

        if (items.length > 0) {
            // Update digit cache in DB
            var callDigits = await CallLegDigitsDao.updateDigits(event.call_leg_ref, event.digitInfo.digit);
            // Search to see if item is collected successfully.  If so, we will need to update DB.
            for (let item of items) {
                let found:boolean = false;
                var call_party = (thisLeg.direction == CallDirection.INBOUND ? CallParty.A_PARTY : CallParty.B_PARTY);
                found = await this.process_item_collection(item, call_party, callDigits, callObj);
                if (found) {
                    // If this item collection applies to both parties, clear the other party buffer too.
                    if (item.call_party == CallParty.ANY) {
                        var otherCallDigits = await CallLegDigitsDao.get(otherLeg.call_leg_ref);
                        if (otherCallDigits != null) {
                            console.log("Clearing other leg digit buffer.  thisLeg=" + thisLeg.call_leg_ref + " otherLeg=" + otherLeg.call_leg_ref);
                            console.log("2. Clearing other leg digit buffer.  thisLeg=" + callDigits.call_leg_ref + " otherLeg=" + otherCallDigits.call_leg_ref);
                            otherCallDigits = await this.clearDigitBuffer(otherCallDigits);
                        }
                    }
                    break;
                }
            }
        }
        
        return Constants.success({
            'action': 'dtmf_info',
            'call_leg_ref': otherLeg.call_leg_ref,
            'sip_call_id': otherLeg.sip_call_id,
            'method': 'info',
            'profile': otherLeg.profile,
            'dtmf': SystemUtils.encode_digits(outDigit),
            'duration': dtmf_duration
        });
    }

    async clearDigitBuffer(callDigits: CallLegDigits): Promise<CallLegDigits> {
        callDigits.digits = "";
        return await CallLegDigitsDao.save(callDigits);
    }

    async process_item_collection(item:ItemConfiguration, call_party: CallParty, callDigits: CallLegDigits, callObj: Call): Promise<boolean> {
        if (item.call_party === call_party || item.call_party == CallParty.ANY) {
            const regex = RegExp(item.pattern);
            var match = regex.test(callDigits.digits);
            // Found the matching item.
            if (match) {

                if (item.replacement != null && item.replacement.length > 0) {
                    console.log("Before replacement " + callDigits.digits);
                    callDigits.digits = callDigits.digits.replace(regex, item.replacement);
                    console.log("After replacement " + callDigits.digits);
                }

                // Save the item to DB.
                SessionItemDao.add(callObj.call_ref, item.id, callDigits.digits);
                console.log("Found matching item: " + item.name + " for call: " + callObj.call_ref);

                // Look for next item or stage to collect.
                if (item.next_item != null) {
                    // If the next item is specified, move on to the next item.
                    callObj.current_item = item.next_item;
                    callObj.current_stage = item.stage;
                } else {
                    // Otherwise, move on to the next stage.
                    callObj.current_item = null;
                    callObj.current_stage = item.stage + 1;
                }

                // See if the next stage exists.
                var stage = await StageConfigurationDao.findByStage(callObj.current_stage);
                if (stage == null) {
                    console.log("Next stage " + callObj.current_stage + " isn't configured.  Will stop digit collection and restore to normal mode.");
                    callObj.current_item = null;
                    callObj.current_stage = null;
                    callObj.secure = false;
                } else {
                    // Update the secure mode.
                    callObj.secure = (stage.mode === StageMode.SECURE);
                }

                // Update call object.
                callObj = await CallDao.save(callObj);

                // Clear the digits buffer now
                callDigits = await this.clearDigitBuffer(callDigits);
                return true;
            }
        }
        return false;
    }
}