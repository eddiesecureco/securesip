import { SipEvent } from "../model/sip-event";
import { EventResponse } from "../model/event-response";

export interface EventHandler {
    handle(event: SipEvent): Promise<EventResponse>;
}