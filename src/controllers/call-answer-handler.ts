import { EventHandler } from "./event-handler";
import { SipEvent } from "../model/sip-event";
import { EventResponse } from "../model/event-response";
import { Constants } from "../utils/constants";

export class CallAnswerHandler implements EventHandler {
    async handle(event: SipEvent): Promise<EventResponse> {
        // TODO: Update call record for CDR.
        console.log("process_call_answer");
        return Constants.success_resp;
    }
}