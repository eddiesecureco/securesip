import { EventHandler } from "./event-handler";
import { SipEvent } from "../model/sip-event";
import { EventResponse } from "../model/event-response";
import { Constants } from "../utils/constants";

export class CallReleaseHandler implements EventHandler {
    async handle(event: SipEvent): Promise<EventResponse> {
        // TODO: Update call record for CDR.
        console.log("process_call_release");

        // TODO: Decide when to delete the DB entries so leave them there forever?
        // var callLegObj = await CallLegDao.get(event.call_leg_ref);
        // var otherLegObj = await CallLegDao.getOtherLegs(event.call_ref, event.call_leg_ref);

        // console.log("callLegObj found=" + (callLegObj != null));
        // console.log("otherLegObj found=" + (otherLegObj != null));

        // console.log("Remove call leg with ref=" + event.call_leg_ref);
        // await CallLegDao.remove(event.call_leg_ref);
        // await CallLegDigitsDao.remove(event.call_leg_ref);

        // // If the other leg has already been deleted, then delete the call object too.
        // if (callLegObj != null && otherLegObj == null) {
        //     if (otherLegObj == null) {    
        //         console.log("Remove call with id=" + callLegObj.call_id);
        //         await CallDao.remove(callLegObj.call_id);
        //     } 
        // } 
        return Constants.success_resp;
    }
}