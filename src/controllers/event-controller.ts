import { Constants } from "../utils/constants";
import { SipEvent } from "../model/sip-event"
import { SipEventType } from "../model/sip-event-type";
import { EventHandler } from "./event-handler";
import { CallCreateHandler } from "./call-create-handler";
import { CallAnswerHandler } from "./call-answer-handler";
import { CallReleaseHandler } from "./call-release-handler";
import { CallDigitHandler } from "./call-digit-handler";
import { EventResponse } from "../model/event-response";

export module EventController {
    const handlers: Map<string, EventHandler> = new Map([
        [SipEventType.CALL_CREATE, new CallCreateHandler()],
        [SipEventType.ANSWER, new CallAnswerHandler()],
        [SipEventType.DIGIT, new CallDigitHandler()],
        [SipEventType.RELEASE, new CallReleaseHandler()]
    ]);

    export async function process_event(event:SipEvent): Promise<EventResponse> {
        if (handlers.has(event.event)) {
            return await handlers.get(event.event).handle(event);
        }
        return Constants.failure_resp;
    };
}