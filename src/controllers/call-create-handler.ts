import { EventHandler } from "./event-handler";
import { SipEvent } from "../model/sip-event";
import { EventResponse } from "../model/event-response";
import { Constants } from "../utils/constants";
import { CallDao } from "../database/call-dao";
import { CallLegDao } from "../database/call-leg-dao";
import { StageConfigurationDao } from "../database/stage-configuration-dao";

export class CallCreateHandler implements EventHandler {
    async handle(event: SipEvent): Promise<EventResponse> {
        console.log("process_call_create call_ref=" + event.call_ref);

        // Check to see if the call has already been created.
        var callObj = await CallDao.get(event.call_ref);
        if (callObj == null) {
            // Find the first item to collect (such as CRN, credit card, CVV, expiry, etc)
            var stage = await StageConfigurationDao.findByStage(1);
            if (stage == null) {
                console.log("ERROR! Stage configuration is not properly configured!");
                return Constants.failure_resp;
            }

            // Create the call that link to the session.
            callObj = await CallDao.add(event.call_ref, stage);
        }

        var callLegObj = await CallLegDao.findOrCreate(callObj.id, event.direction, event.profile, event.call_leg_ref, event.sip_call_id);
        console.log("Created call leg=" + callLegObj.id + " -> Call ref " + callObj.call_ref);

        return Constants.success_resp;
    }
}