
import { EventResponse } from "../model/event-response";
export module Constants {
    export const success_resp = <EventResponse> {
        statusCode: 200,
        body: JSON.stringify({"action": "none"})
    }

    export const failure_resp = <EventResponse> {
        statusCode: 503,
        body: JSON.stringify({"action": "none"})
    }

    export function success(body:any) {
        return <EventResponse> {
            statusCode: 200,
            body: JSON.stringify(body)
        };
    }

}