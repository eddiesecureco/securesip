
export module SystemUtils {

    export function getSystemConfig(config: Map<string, string>, key: string, defaultVal: string): string {
        return config.has(key) ? config.get(key) : defaultVal;
    }

    /**
     * Convert Sip info event to digits.
     * See https://www.voip-info.org/sip-dtmf-signalling
     * NOTE: We only capture 0-9,*,#.  The rest will be ignored.
     * Event Encoding   <--->   Actual DTMF
     * 0-9                      0-9
     * 10                       *
     * 11                       #
     * 12-15                    A-D
     * 16                       Flash  
     * @param {*} digitEvent
     */
    export function decode_digits(digitEvent:number):string {
        if (digitEvent >= 0 && digitEvent <= 9) {
            return '' + digitEvent;     // to string
        } else if (digitEvent == 10) {
            return '*';
        } else if (digitEvent == 11) {
            return '#';
        } else {
            return '';
        }
    }

    /**
     * Convert digits to SIP Info encoding.
     * Opposite to @function decode_digits`.
     * 
     * @param {*} digit 
     */
    export function encode_digits(digit:string):number {
        var d = Number(digit);
        if (d >= 0 && d <= 9) {
            return d;
        } else if (digit == '*') {
            return 10;
        } else if (digit == '#') {
            return 11;
        } else {
            return 99;  // invalid dtmf key
        }
    }
}